import Vuex from 'vuex'
import { createLocalVue } from '@vue/test-utils'
import cloneDeep from 'lodash.clonedeep'
import index from '~/store'

describe('store/index.js', () => {
  beforeEach(() => {
    // メモリ上に仮想のVueインスタンスを生成する
    const localVue = createLocalVue()
    localVue.use(Vuex)
  })

  describe('mutations', () => {
    test('increment ミューテーションがコミットされると、 count ステートの値が +1 される)', () => {
      const store = new Vuex.Store(cloneDeep(index))
      expect(store.getters.count).toBe(0)
      store.commit('increment')
      expect(store.getters.count).toBe(1)
    })
  })

  describe('actions', () => {
    test('increment アクションを dispatch するたびに、 increment ミューテーションがコミットされる', () => {
      // incrementミューテーションをモックしてVuexストアを構築する
      const mutations = {
        increment: jest.fn()
      }
      const store = new Vuex.Store({ ...cloneDeep(index), mutations })

      store.dispatch('increment')
      expect(mutations.increment).toHaveBeenCalled()
    })
  })
})
